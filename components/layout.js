import React, { useContext } from 'react';
import Head from 'next/head';
import NextLink from 'next/link';
import {
  AppBar,
  Container,
  createTheme,
  CssBaseline,
  Link,
  ThemeProvider,
  Toolbar,
  Typography,
  Badge,
  Button,
  InputBase,
} from '@material-ui/core';
import useStyles from '../utils/styles';
import { Search } from '@mui/icons-material';
import { Store } from '../utils/Store';
import { IconButton } from '@material-ui/core';
import Cookies from 'js-cookie';

export default function layout({ title, description, children }) {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { state, dispatch } = useContext(Store);
  const { darkMode, cart } = state;
  const theme = createTheme({
    typography: {
      h1: {
        fontSize: '2rem',
        fontWeight: 700,
        margin: '1rem 0',
      },
      h2: {
        fontSize: '1.4rem',
        fontWeight: 400,
        margin: '1rem 0',
      },
    },
    palette: {
      type: 'light',
      primary: {
        main: '#00ed64',
      },
      secondary: {
        main: '#f694c1',
      },
    },
  });
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const classes = useStyles();
  return (
    <div>
      <Head>
        <title>{title ? `${title} - Silmaribook` : 'Silmaribook'}</title>
        {description && <meta name="description" content={description}></meta>}
      </Head>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <AppBar position="static" className={classes.navbar}>
          <Toolbar>
            <NextLink href="/" passHref>
              <Link>
                <Typography className={classes.brand}>Silmaribook</Typography>
              </Link>
            </NextLink>
            <div className={classes.grow}></div>
            <div className={classes.searchSection}>
              <form className={classes.searchForm}>
                <InputBase
                  name="query"
                  className={classes.searchInput}
                  placeholder="Search products"
                />
                <IconButton
                  type="submit"
                  className={classes.iconButton}
                  aria-label="search">
                  <Search />
                </IconButton>
              </form>
            </div>
            <div>
              <NextLink href="/cart" passHref>
                <Link>
                  {cart.cartItems.length > 0 ? (
                    <Badge
                      color="secondary"
                      badgeContent={cart.cartItems.length}>
                      Cart
                    </Badge>
                  ) : (
                    'Cart'
                  )}
                </Link>
              </NextLink>
              <NextLink href="/login" passHref>
                <Link>Login</Link>
              </NextLink>
            </div>
          </Toolbar>
        </AppBar>
        <Container className={classes.main}>{children}</Container>
        <footer className={classes.footer}>
          <Typography>All right reserved Silmaribook.</Typography>
        </footer>
      </ThemeProvider>
    </div>
  );
}
