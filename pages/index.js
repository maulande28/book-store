import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Typography,
} from '@material-ui/core';
import Head from 'next/head';
import Image from 'next/image';
import Layout from '../components/layout';
import data from '../utils/data';
import NextLink from 'next/link';
import styles from '../styles/Home.module.css';
import db from '../utils/db';
import Product from '../models/Product';

const imgStyles = (theme) => ({
  Card: {
    width: 300,
    margin: 'auto',
  },
  Media: {
    height: 550,
    width: '100%',
    objectFit: 'cover',
  },
});

const imgStyle = {
  height: 450,
};

export default function Home(props) {
  const { products } = props;
  return (
    <Layout>
      <div>
        <h1>Products</h1>
        <Grid container spacing={3}>
          {products.map((product) => (
            <Grid item md={3} key={product.name}>
              <Card>
                <NextLink href={`/product/${product.slug}`} passHref>
                  <CardActionArea>
                    <CardMedia
                      style={imgStyle}
                      component="img"
                      image={product.image}
                      title={product.name}></CardMedia>
                    <CardContent>
                      <Typography>{product.name}</Typography>
                    </CardContent>
                  </CardActionArea>
                </NextLink>
                <CardActions>
                  <Typography>${product.price}</Typography>
                  <Button
                    size="small"
                    variant="contained"
                    color="primary"
                    style={{
                      borderRadius: 10,
                      fontWeight: 800,
                    }}>
                    Add to cart
                  </Button>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </div>
    </Layout>
  );
}

export async function getServerSideProps() {
  await db.connect();
  const products = await Product.find({}).lean();
  await db.disconnect();
  return {
    props: {
      products: products.map(db.convertDocToObj),
    },
  };
}
